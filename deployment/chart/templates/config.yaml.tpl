apiVersion: v1
kind: ConfigMap
metadata:
  name: image-config
data:
  images-v1.yaml: |
{{ .Values.images | toYaml | indent 4 }}
