apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: image-config-viewer
rules:
  - apiGroups: [""]
    resources: [configmaps]
    resourceNames: [image-config]
    verbs: [get, watch]
