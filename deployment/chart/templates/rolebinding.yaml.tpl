apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: image-config-viewer-all
subjects:
  - kind: Group
    name: system:authenticated
    apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: image-config-viewer
  apiGroup: rbac.authorization.k8s.io
