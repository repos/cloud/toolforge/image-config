# image-config

This repository defines which images are available for Toolforge users
via the `webservice` and `jobs` entrypoints. The important data is in
two files:

- `deployment/chart/values.yaml` contains the defined images.
- `schema.json` contains the [schema] definition for the values file.

Please see the [documentation] for instructions on how to deploy
changes.

[schema]: https://json-schema.org/
[documentation]: https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Kubernetes#Managing_images_available_for_tools

## License

GNU GPLv3 only, see LICENSE for full terms.

Copyright (c) 2022- Taavi Väänänen, Wikimedia Foundation and contributors.
