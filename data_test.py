import json

import pytest
import yaml
from jsonschema import validate


@pytest.fixture
def schema() -> dict:
    with open("schema.json", "r") as f:
        schema_data = json.load(f)
    return schema_data


def get_images():
    with open("deployment/chart/values.yaml", "r") as f:
        data = yaml.safe_load(f)

    return [pytest.param(data, id=name) for name, data in data["images"].items()]


@pytest.mark.parametrize(["data"], get_images())
def test_schema_validity(schema, data):
    validate(data, schema)
